Source: ignition-common
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jose Luis Rivero <jrivero@osrfoundation.org>
Section: science
Priority: optional
Build-Depends: cmake,
               pkg-config,
               debhelper-compat (= 13),
               doxygen,
               libgtest-dev,
               libfreeimage-dev,
               libignition-cmake-dev (>= 2.14.0),
               libignition-math-dev (>= 6.12.0),
               libignition-utils-dev,
               libtinyxml2-dev,
               uuid-dev,
               libgts-dev,
               libavformat-dev,
               libavcodec-dev,
               libavdevice-dev,
               libswscale-dev,
               libtinyobjloader-dev,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/science-team/ignition-common
Vcs-Git: https://salsa.debian.org/science-team/ignition-common.git
Homepage: https://ignitionrobotics.org/libs/common

Package: libignition-common-core-dev
Architecture: any
Section: libdevel
Depends: libignition-cmake-dev (>= 2.14.0),
         uuid-dev,
         libignition-common4-4 (= ${binary:Version}),
         libtinyobjloader-dev,
         ${misc:Depends}
Suggests: ignition-tools
Breaks: libignition-fuel-tools-dev (<< 7)
Multi-Arch: same
Description: Collection of useful code used by robotics apps - Core dev files
 Ignition common is a component in the Ignition framework, a set of
 libraries designed to rapidly develop robot applications. A collection of
 useful classes and functions for handling many command tasks. This includes
 parsing 3D mesh files, managing console output, and using PID controllers.
 .
 Core development files

Package: libignition-common-dev
Architecture: any
Section: libdevel
Depends: libignition-common-core-dev (= ${binary:Version}),
         libignition-common-av-dev (= ${binary:Version}),
         libignition-common-graphics-dev (= ${binary:Version}),
         libignition-common4-profiler4 (= ${binary:Version}),
         libignition-common4-events4 (= ${binary:Version}),
         ${misc:Depends}
Multi-Arch: same
Recommends: ignition-common-cli
Description: Collection of useful code used by robotics apps - Metapackage
 Ignition common is a component in the Ignition framework, a set of
 libraries designed to rapidly develop robot applications. A collection of
 useful classes and functions for handling many command tasks. This includes
 parsing 3D mesh files, managing console output, and using PID controllers.
 .
 Metapackage for development. Includes events and profiler.

Package: libignition-common4-4
Architecture: any
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Collection of useful code used by robotics apps - Shared library
 Ignition common is a component in the Ignition framework, a set of
 libraries designed to rapidly develop robot applications. A collection of
 useful classes and functions for handling many command tasks. This includes
 parsing 3D mesh files, managing console output, and using PID controllers.
 .
 This package gathers the shared library

Package: libignition-common4-av4
Architecture: any
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Collection of useful code used by robotics apps - AV libraries
 Ignition common is a component in the Ignition framework, a set of
 libraries designed to rapidly develop robot applications. A collection of
 useful classes and functions for handling many command tasks. This includes
 parsing 3D mesh files, managing console output, and using PID controllers.
 .
 AV component of the library

Package: libignition-common-av-dev
Architecture: any
Section: libdevel
Depends: libignition-cmake-dev (>= 2.14.0),
         libignition-common-core-dev,
         libavdevice-dev,
         libavformat-dev,
         libavcodec-dev,
         libswscale-dev,
         libavutil-dev,
         libignition-common4-av4 (= ${binary:Version}),
         ${misc:Depends}
Multi-Arch: same
Description: Collection of useful code used by robotics apps - AV dev files
 Ignition common is a component in the Ignition framework, a set of
 libraries designed to rapidly develop robot applications. A collection of
 useful classes and functions for handling many command tasks. This includes
 parsing 3D mesh files, managing console output, and using PID controllers.
 .
 AV component of the library, development files

Package: libignition-common4-events4
Architecture: any
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Collection of useful code used by robotics apps - Events libraries
 Ignition common is a component in the Ignition framework, a set of
 libraries designed to rapidly develop robot applications. A collection of
 useful classes and functions for handling many command tasks. This includes
 parsing 3D mesh files, managing console output, and using PID controllers.
 .
 Events component of the library

Package: libignition-common-graphics-dev
Architecture: any
Section: libdevel
Depends: libignition-cmake-dev (>= 2.14.0),
         libignition-common-core-dev,
         libignition-math-dev (>= 6.12.0),
         libignition-utils-dev,
         libtinyobjloader-dev,
         libtinyxml2-dev,
         libgts-dev,
         libignition-common4-graphics4 (= ${binary:Version}),
         ${misc:Depends}
Multi-Arch: same
Description: Ignition Common classes and functions (Graphics) - Development files
 Ignition Common is a component in the ignition framework, a set of libraries
 designed to rapidly develop robot applications.
 .
 Graphics component of the library, development files

Package: libignition-common4-graphics4
Architecture: any
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Collection of useful code used by robotics apps - Graphics libraries
 Ignition common is a component in the Ignition framework, a set of
 libraries designed to rapidly develop robot applications. A collection of
 useful classes and functions for handling many command tasks. This includes
 parsing 3D mesh files, managing console output, and using PID controllers.
 .
 Graphics component of the library

Package: libignition-common4-profiler4
Architecture: any
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Collection of useful code used by robotics apps - Profiler libs
 Ignition common is a component in the Ignition framework, a set of
 libraries designed to rapidly develop robot applications. A collection of
 useful classes and functions for handling many command tasks. This includes
 parsing 3D mesh files, managing console output, and using PID controllers.
 .
 Profiler component of the library

Package: ignition-common-cli
Architecture: any
Depends: libignition-common4-4 (= ${binary:Version}),
         ${misc:Depends}
Multi-Arch: same
Description: Collection of useful code used by robotics apps - CLI tools
 Ignition common is a component in the Ignition framework, a set of
 libraries designed to rapidly develop robot applications. A collection of
 useful classes and functions for handling many command tasks. This includes
 parsing 3D mesh files, managing console output, and using PID controllers.
 .
 Ignition transport client tools
